export interface User {
    id?: string;
    name: string;
    email: Publisher;
    alt_img?: string;
}

export enum Publisher {
    DCComics = "DC Comics",
    MarvelComics = "Marvel Comics",
}
