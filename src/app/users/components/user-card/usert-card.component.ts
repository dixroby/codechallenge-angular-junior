import { Component, Input } from '@angular/core';
import { User } from '../../interfaces/user.interface';

@Component({
  selector: 'app-user-tarjeta',
  templateUrl: './user-card.component.html',
  styles: [`
  mat-card {
    margin-top: 20px;
  }
`]
})
export class UserCardComponent {

  @Input() user!: User;

}
