import { Component, OnInit } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { User } from '../../interfaces/user.interface';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styles: [
  ]
})
export class BuscarComponent implements OnInit {

  argument: string = '';
  listUsers: User[] = [];
  userSelected: User | undefined;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }


  buscando() {

    this.userService.getUsers()
      .subscribe(users => this.listUsers = users);

  }

  opcionSeleccionada(event: MatAutocompleteSelectedEvent) {

    if (!event.option.value) {
      this.userSelected = undefined;
      return;
    }

    const user: User = event.option.value;
    this.argument = user.name;

    this.userService.getUserById(user.id!)
      .subscribe(user => this.userSelected = user);
  }

}
